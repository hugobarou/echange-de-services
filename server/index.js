const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const mysql = require("mysql");
const config = require("./config");
const fileUpload = require("express-fileupload");

const db = mysql.createConnection(config.mysql);

db.connect((err) => {
  if (err) console.log(err);
  else console.log("MySQL connection successfully opened!");
});

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
// To upload profile images
app.use(fileUpload());

app.get("/api/getServices", (req, res) => {
  const sqlSELECT = "SELECT * FROM service";
  db.query(sqlSELECT, (err, result) => {
    console.log(result);
    res.send(result);
  });
});
app.get("/api/getServicesByCategories/:categoryId", (req, res) => {
  const sqlSELECT = `SELECT * FROM service WHERE categoryID=${req.params.categoryId}`;
  db.query(sqlSELECT, (err, result) => {
    console.log(result);
    res.send(result);
  });
});

app.get("/api/getCategories", (req, res) => {
  const sqlSelect = "SELECT * FROM categories";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

app.get("/api/get", (req, res) => {
  const sqlSELECT = "SELECT * FROM account";
  db.query(sqlSELECT, (err, result) => {
    res.send(result);
  });
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});
app.get("/api/findItem/:desc", (req, res) => {
  const desc = req.params.desc;
  //const sqlSELECT = `SELECT * FROM item WHERE itemName LIKE '%${desc}%'`;
  const sqlSELECT = `SELECT item.*, service.* FROM item RIGHT JOIN service on service.id=item.serviceID WHERE item.itemName LIKE '%${desc}%'`;
  db.query(sqlSELECT, (err, result) => {
    //console.log(result);
    res.send(result);
  });
});
/**
app.post("/api/insertAccount", (req, res) => {
  const accountFirstName = req.body.accountFirstName;
  const accountLastName = req.body.accountLastName;
  const accountEmail = req.body.accountEmail;
  const accountAddress = req.body.accountAddress;
  const accountPassword = req.body.accountPassword;
  const accountTel = req.body.accountTel;
	
  const sqlInsertAccount = "INSERT INTO account (firstName, lastName, email, address, password, tel) VALUES (?, ?, ?, ?, ?, ?)";
  db.query(sqlInsertAccount, [accountFirstName, accountLastName, accountEmail, accountAddress, accountPassword, accountTel], (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});
**/
app.put("/api/updateAccount", (req, res) => {
  const accountID = req.body.accountID;
  const accountFirstName = req.body.accountFirstName;
  const accountLastName = req.body.accountLastName;
  const accountEmail = req.body.accountEmail;
  const accountAddress = req.body.accountAddress;
  const accountPassword = req.body.accountPassword;
  const accountTel = "NOTELNOTEL";
  console.log(req.body);

  const sqlUpdateAccount =
    "UPDATE account SET firstName = ?, lastName = ?, email = ?, address = ?, password = ?, tel = ? WHERE id = ?";
  db.query(
    sqlUpdateAccount,
    [
      accountFirstName,
      accountLastName,
      accountEmail,
      accountAddress,
      accountPassword,
      accountTel,
      accountID,
    ],
    (err, result) => {
      console.log(result); // TEST ONLY, REMOVE WHEN DONE
    }
  );
});

app.post("/api/getAccountByID", (req, res) => {
  const accountID = req.body.accountID;
  console.log(req.body);

  const sqlGetAccountByID = "SELECT * from account WHERE id = ?"; // Add way to get by ID or name or something
  db.query(sqlGetAccountByID, accountID, (err, result) => {
    res.send(result);
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.get("/api/getAccountByName", (req, res) => {
  const accountFirstName = req.body.accountFirstName;
  const accountLastName = req.body.accountLastName;

  const sqlGetAccountByName =
    "SELECT * from account WHERE firstName = ? AND lastName = ?"; // Add way to get by ID or name or something
  db.query(
    sqlGetAccountByName,
    [accountFirstName, accountLastName],
    (err, result) => {
      console.log(result); // TEST ONLY, REMOVE WHEN DONE
    }
  );
});

app.get("/api/getAccountByAddress", (req, res) => {
  const accountAddress = req.body.accountAddress;

  const sqlGetAccountByAddress = "SELECT * from account WHERE address = ?"; // Add way to get by ID or name or something
  db.query(sqlGetAccountByAddress, accountAddress, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.get("/api/getAccountByEmail", (req, res) => {
  const accountEmail = req.body.accountEmail;

  const sqlGetAccountByEmail = "SELECT * from account WHERE email = ?"; // Add way to get by ID or name or something
  db.query(sqlGetAccountByEmail, accountEmail, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.get("/api/getAccounts", (req, res) => {
  const sqlSelectAccount = "SELECT * from account";
  db.query(sqlSelectAccount, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.post("/api/getAccountByLogin", (req, res) => {
  const accountEmail = req.body.accountEmail;
  const accountPassword = req.body.accountPassword;

  const sqlSelectAccount =
    "SELECT * from account WHERE email = ? AND password = ?";
  db.query(sqlSelectAccount, [accountEmail, accountPassword], (err, result) => {
    res.send(result);
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.post("/api/insertOrder", (req, res) => {
  const orderStatus = "New";
  const account_id = req.body.account_id;
  const offer_id = req.body.offer_id;
  const vendor_id = req.body.vendor_id;

  const sqlInsertCommande =
    "INSERT INTO order (orderStatus, account_id, offer_id, vendor_id) VALUES (?, ?, ?, ?)";
  db.query(
    sqlInsertAccount,
    [orderStatus, account_id, offer, id, vendor_id],
    (err, result) => {
      console.log(result); // TEST ONLY, REMOVE WHEN DONE
    }
  );
});

app.get("/api/getOrders", (req, res) => {
  const sqlGetOrders = "SELECT * from order";
  db.query(sqlGetOrders, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.post("/api/getOrdersByAccount", (req, res) => {
  const account_id = req.body.account_id;
  const sqlGetOrdersByAccount = "SELECT * from order WHERE account_id = ?";
  db.query(sqlGetOrders, account_id, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.post("/api/getOrdersByOffer", (req, res) => {
  const offer_id = req.body.offer_id;
  const sqlGetOrdersByOffer = "SELECT * from order WHERE offer_id = ?";
  db.query(sqlGetOrdersByOffer, offer_id, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.post("/api/getOrdersByVendor", (req, res) => {
  const sqlGetOrdersByVendor = "SELECT * from order WHERE vendor_id = ?";
  db.query(sqlGetOrdersByVendor, vendor_id, (err, result) => {
    console.log(result); // TEST ONLY, REMOVE WHEN DONE
  });
});

app.delete("/api/deleteOrders", (req, res) => {
  const sqlDeleteOrders = "DELETE * FROM order";

  db.query(sqlDeleteOrders, (err, result) => {
    console.log(result);
  });
});

app.delete("/api/deleteOrderByID", (req, res) => {
  const order_id = req.body.order_id;
  const sqlDeleteOrderByID = "DELETE FROM order WHERE id = ?";

  db.query(sqlDeleteOrderByID, order_id, (err, result) => {
    console.log(result);
  });
});

app.post("/api/createAccount", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const address = req.body.address;
  const password = req.body.password;
  const tel = req.body.tel;

  const sqlInsert =
    "INSERT INTO account (firstName, lastName, email, address, password, tel) VALUES (?,?,?,?,?,?)";
  db.query(
    sqlInsert,
    [firstName, lastName, email, address, password, tel],
    (err, result) => {
      console.log(err);
    }
  );
});
app.delete("/api/delete/:id", (req, res) => {
  const id = req.params.id;
  const sqlDelete = "DELETE FROM account WHERE id = ?";

  db.query(sqlDelete, id, (err, result) => {
    if (err) console.log(err);
  });
});

app.post("/upload", (req, res) => {
  const paths = [];
  for (img in req.files) {
    let file = req.files[img];
    paths.push(`/uploads/${file.name}`);
    file.mv(`../public/uploads/${file.name}`, (err) => {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
    });
  }
  const accountId = req.body.accountId;
  const sqlInsert =
    "INSERT INTO vendor (profile_img, identity_img, eligible_img, account_id) VALUES (?,?,?,?)";

  db.query(
    sqlInsert,
    [paths[0], paths[1], paths[2], accountId],
    (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
      res.status(200).send(result);
    }
  );
});

app.put("/api/updateVendor", (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: "No file was selected" });
  }
  const avatar = req.files.avatar;
  const accountId = req.body.accountId;
  const path = `/uploads/${avatar.name}`;
  const sqlUpdate = "UPDATE vendor SET profile_img = ? WHERE account_id = ?";

  avatar.mv(`../public/uploads/${avatar.name}`, (err) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
  });

  db.query(sqlUpdate, [path, accountId], (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    return res.status(200).json({ msg: "The request has succeeded" });
  });
});

app.get("/api/getVendor/:id", (req, res) => {
  const sqlSelect = `SELECT * FROM vendor WHERE account_id = ${req.params.id}`;
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    console.log(result);
    res.status(200).send(result);
  });
});

app.delete("/api/deleteVendor/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const sqlDeleteVendor = `DELETE FROM vendor WHERE account_id = ${id}`;
  db.query(sqlDeleteVendor, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    console.log(result);
    return res.status(200).json({ msg: "The request has succeeded" });
  });
});

app.get("/api/getCategories", (req, res) => {
  const sqlSelect = "SELECT * FROM categories";
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.post("/api/insertService", (req, res) => {
  const name = req.body.serviceName;
  const desc = req.body.serviceDesc;
  const category = parseInt(req.body.categoryID);
  const vendorID = parseInt(req.body.vendorID);
  const location = req.body.location;
  console.log("TEST NEW Service");

  const sqlInsert =
    "INSERT INTO service (serviceName,serviceDesc,categoryID,vendorID,location) VALUES (?,?,?,?,?)";
  db.query(
    sqlInsert,
    [name, desc, category, vendorID, location],
    (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).send({ msg: err.message });
      }
      res.status(200).send(result);
    }
  );
});

app.get("/api/getService/:id", (req, res) => {
  console.log(req.params.id);
  const sqlSelect = `SELECT * FROM service WHERE vendorID = ${req.params.id}`;
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    console.log(result);
    res.status(200).send(result);
  });
});

app.get("/api/getCategories", (req, res) => {
  const sqlSelect = "SELECT * FROM categories";
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.get("/api/getCategoryName/:category", (req, res) => {
  const sqlSelect = `SELECT * FROM categories WHERE id = ${req.params.category}`;
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.delete("/api/deleteService/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const sqlDelete = `DELETE FROM service WHERE id = ${id}`;
  db.query(sqlDelete, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    return res.status(200).json({ msg: "The request has succeeded" });
  });
});

app.put("/api/updateService", (req, res) => {
  const serviceID = req.body.serviceID;
  const name = req.body.serviceName;
  const desc = req.body.serviceDesc;
  const category = parseInt(req.body.categoryID);
  const location = req.body.location;

  const sqlUpdate =
    "UPDATE service SET serviceName = ?, serviceDesc = ?, categoryID = ?, location = ? WHERE id = ?";
  db.query(
    sqlUpdate,
    [name, desc, category, location, serviceID],
    (err, result) => {
      if (err) {
        return res.status(500).send({ msg: err.message });
      }
      return res.status(200).json({ msg: "The request has succeeded" });
    }
  );
});

app.get("/api/getItems/:id", (req, res) => {
  const sqlSelect = `SELECT * FROM item WHERE serviceID = ${req.params.id}`;
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.post("/api/insertItem", (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: "No file was selected" });
  }
  const picture = req.files.picture;
  const path = `/uploads/${picture.name}`;
  const name = req.body.itemName;
  const desc = req.body.itemDesc;
  const price = parseFloat(req.body.itemPrice);
  const serviceID = parseInt(req.body.serviceID);

  picture.mv(`../public/uploads/${picture.name}`, (err) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
  });

  const sqlInsert =
    "INSERT INTO item (itemPicture,itemName,itemDesc,itemPrice,serviceID) VALUES (?,?,?,?,?)";
  db.query(sqlInsert, [path, name, desc, price, serviceID], (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.delete("/api/deleteItem/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const sqlDelete = `DELETE FROM item WHERE id = ${id}`;
  db.query(sqlDelete, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.put("/api/updateItem", (req, res) => {
  const name = req.body.itemName;
  const desc = req.body.itemDesc;
  const price = parseFloat(req.body.itemPrice);
  const itemId = parseInt(req.body.itemId);

  const sqlUpdate =
    "UPDATE item SET itemName = ?, itemDesc = ?, itemPrice = ? WHERE id = ?";
  const sqlUpdateImage =
    "UPDATE item SET itemPicture = ?, itemName = ?, itemDesc = ?, itemPrice = ? WHERE id = ?";

  if (req.files != null) {
    const picture = req.files.picture;
    const path = `/uploads/${picture.name}`;
    picture.mv(`../public/uploads/${picture.name}`, (err) => {
      if (err) {
        return res.status(500).send({ msg: err.message });
      }
    });
    db.query(
      sqlUpdateImage,
      [path, name, desc, price, itemId],
      (err, result) => {
        if (err) {
          return res.status(500).send({ msg: err.message });
        }
        return res.status(200).json({ msg: "The request has succeeded" });
      }
    );
  } else {
    db.query(sqlUpdate, [name, desc, price, itemId], (err, result) => {
      if (err) {
        return res.status(500).send({ msg: err.message });
      }
      return res.status(200).json({ msg: "The request has succeeded" });
    });
  }
});

app.get("/api/getFavorites/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const sqlSelect = `SELECT * FROM service WHERE id IN (SELECT serviceId FROM favorite WHERE accountId = ${id})`;
  db.query(sqlSelect, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});
app.post("/api/deleteFavorite", (req, res) => {
  const accountId = parseInt(req.body.accountId);
  const serviceId = parseInt(req.body.serviceId);
  const sqlDelete = `DELETE FROM favorite WHERE accountId = ${accountId} AND serviceId = ${serviceId}`;
  db.query(sqlDelete, (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});
app.post("/api/insertFavorite", (req, res) => {
  const accountId = parseInt(req.body.accountId);
  const serviceId = parseInt(req.body.serviceId);
  const sqlInsert = "INSERT INTO favorite (accountId,serviceId) VALUES (?,?)";
  db.query(sqlInsert, [accountId, serviceId], (err, result) => {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    res.status(200).send(result);
  });
});

app.listen(3001, () => {
  console.log("Running on port 3001");
});
