import React, { useState } from "react";

import Navbar from "./components/Navbar";
import Home from "./components/pages/Home";
import About from "./components/pages/About";
import SignUp from "./components/pages/SignUp";
import SignIn from "./components/pages/SignIn";
import LogOut from "./components/pages/LogOut";
import Vendor from "./components/pages/Vendor";
import VendorService from "./components/pages/VendorService";
import Service from "./components/pages/Service";
import Favorite from "./components/pages/Favorite";
import Recherche from "./components/pages/Recherche";

import Items from "./components/pages/Items.js";

import Chat from "./components/pages/Chat";

import Profile from "./components/pages/ProfileClient";
import { AuthContext } from "./context/auth";
// import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import PrivateRoute from "./privateRoute";

function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  };

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <Navbar />
        {/* <div>{console.log(props.catID)}</div> */}
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/sign-up" component={SignUp} />
          <Route path="/sign-in" component={SignIn} />
          <PrivateRoute path="/vendor" component={Vendor} />
          <PrivateRoute path="/vendor-service" component={VendorService} />
          <Route path="/chat" component={Chat} />
          <Route path="/sign-out" component={LogOut} />
          <PrivateRoute path="/profileClient" component={Profile} />
          <Favorite path="/favorite" component={Favorite} />
          <Route path="/services/:id" component={Service} />
          <Route path="/recherche/:desc" component={Recherche} />
          <Route path="/items/:id" component={Items} />
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
