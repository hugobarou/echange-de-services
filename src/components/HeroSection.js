import React from "react";
import { Button } from "./Button";
import "./HeroSection.css";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
    color: "red",
    margin: "150px",
  },
}));

export default function HeroSection() {
  const classes = useStyles();
  return (
    <div className="hero-container">
      <h1>
        CALENDA.
      </h1>
      <p>Le futur est meilleur ensemble</p>
      <div className="hero-btns">
        <Button
          className="btns"
          buttonStyle="btn--outline"
          buttonSize="btn--large"
          to="/sign-up"
        >
          INSCRIVEZ VOUS
        </Button>
      </div>

    </div>
  );
}

// export default HeroSection;
