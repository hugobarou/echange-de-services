/*
 * Programmeur: Hugo Barou
 * Fichier: ItemForm.js
 * Description: Formulaire pour créer un item dans un service.
 */

// Importations
import React, { useState, useEffect } from "react";
import Axios from "axios";
import "./ItemForm.css";
import ItemInfo from "./ItemInfo";

export default function ItemForm(props) {
  const id = props.serviceID;
  const [items, setItems] = useState([]);
  const [picture, setPicture] = useState([]);
  const [name, setName] = useState([""]);
  const [desc, setDesc] = useState([""]);
  const [price, setPrice] = useState([""]);

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getItems/${id}`).then((response) => {
      setItems(response.data);
    });
  }, []);

  const fileSelected = (e) => {
    setPicture(e.target.files[0]);
  };

  const createItem = (e) => {
    e.preventDefault();
    const formData = new FormData();

    formData.append("picture", picture);
    formData.set("itemName", name);
    formData.set("itemDesc", desc);
    formData.set("itemPrice", price);
    formData.set("serviceID", id);

    try {
      Axios.post(`http://localhost:3001/api/insertItem/`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      console.log(err.response.data.msg);
    }
  };

  return (
    <div className="item">
      <div className="container">
        <form className="item-form">
          <label className="form-label" for="itemPicture"></label>
          <input
            type="file"
            id="itemPicture"
            accept="image/png, image/jpeg, image/jpg"
            onChange={fileSelected}
          />
          <label for="itemName">Nom de l'item</label>
          <input
            type="text"
            id="itemName"
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
          <label for="itemDesc">Description de l'item</label>
          <input
            type="text"
            id="itemDesc"
            onChange={(e) => {
              setDesc(e.target.value);
            }}
          />
          <label for="itemPrice">Prix unitaire de l'item</label>
          <input
            type="text"
            id="itemPrice"
            onChange={(e) => {
              setPrice(e.target.value);
            }}
          />
          <input type="submit" value="Créer un item" onClick={createItem} />
          <p>Vos items:</p>
          {items.map((val) => {
            return (
              <ItemInfo
                id={val.id}
                picture={val.itemPicture}
                name={val.itemName}
                desc={val.itemDesc}
                price={val.itemPrice}
                serviceID={val.serviceID}
              />
            );
          })}
        </form>
      </div>
    </div>
  );
}
