import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

function getRandomNumber() {
  return Math.floor(Math.random() * 100);
}

const useStyles = makeStyles((theme) => ({
  root: {
    // maxHeight: 400,
    // display:"flex",
  },
  media: {
    height: 250,
  },
  cardLinks: {
    color: theme.palette.secondary.dark,
  },
  serviceDesc: {
    height: "150px",
  },
  stars: {
    alignItem: "center",
  },
}));

export default function ServicesCardsItems(props) {
  const classes = useStyles();

  return (
    <>
      <Grid item xs={12} sm={5}>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              // image={require('./z0RgblR.jpg')}

              src={`https://source.unsplash.com/random?sig=${getRandomNumber()}`}
              component="img"
              // title="Contemplative Reptile"
            />
            <CardContent className={classes.itemDesc}>
              <Typography gutterBottom variant="h5" component="h2">
                {props.itemName}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {props.itemDesc}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Typography variant="h6" color="textSecondary" component="p">
              {props.itemPrice}$
            </Typography>
          </CardActions>
        </Card>
      </Grid>
    </>
  );
}
