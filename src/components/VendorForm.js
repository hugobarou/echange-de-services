/*
 * Programmeur: Hugo Barou
 * Fichier: VendorForm.js
 * Description: Formulaire pour la création d'un profile vendeur
 */

// Importations
import React, { useState } from "react";
import Axios from "axios";
import "./VendorForm.css";

import { useAuth } from "../context/auth";

export default function VendorForm() {
  const { authTokens } = useAuth();
  const [profile, setProfile] = useState("");
  const [identity, setIdentity] = useState("");
  const [eligible, setEligible] = useState("");
  const accountId = authTokens[0].id;

  const uploadSelectedFiles = async (e) => {
    if (profile && identity && eligible) {
      e.preventDefault();
      const formData = new FormData();
      formData.append("profile", profile);
      formData.append("identity", identity);
      formData.append("eligible", eligible);
      formData.set("accountId", accountId);

      try {
        const res = await Axios.post("http://localhost:3001/upload", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
      } catch (err) {
        if (err.response.status === 500) {
          console.log("There was a problem with the server");
        } else {
          console.log(err.response.data.msg);
        }
      }
    } else {
      console.log("You have to choose a file for every step!");
    }
  };

  const fileSelected = (e) => {
    switch (e.target.id) {
      case "profile":
        setProfile(e.target.files[0]);
        break;
      case "identity":
        setIdentity(e.target.files[0]);
        break;
      case "eligible":
        setEligible(e.target.files[0]);
        break;
      default:
        console.log("You must choose a file!");
    }
  };

  return (
    <>
      <div className="vendor-form">
        <div className="formulaire">
          <form>
            <label className="form-label" for="profile">
              Étape 1:
            </label>
            <p>Télécharger une photo de profil</p>
            <input
              type="file"
              id="profile"
              accept="image/png, image/jpeg, image/jpg"
              onChange={fileSelected}
            />
            <label className="form-label" for="identity">
              Étape 2:
            </label>
            <p>Télécharger une photo d'identité</p>
            <input
              type="file"
              id="identity"
              accept="image/png, image/jpeg, image/jpg"
              onChange={fileSelected}
            />
            <label className="form-label" for="eligible">
              Étape 3:
            </label>
            <p>Téléchargez votre photo d'éligibilité au travail</p>
            <input
              type="file"
              id="eligible"
              accept="image/png, image/jpeg, image/jpg"
              onChange={fileSelected}
            />
            <input
              type="submit"
              value="Créer le profile"
              onClick={uploadSelectedFiles}
              className="btn-sv"
            />
          </form>
        </div>
      </div>
    </>
  );
}
