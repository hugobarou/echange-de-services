import Axios from "axios";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import "./Cards.css";
import Grid from "@material-ui/core/Grid";
import ServiceCardItem from "./ServicesCardsItems.js";
import Divider from "@material-ui/core/Divider";

// const [listeServices, setListeServices] = useState([]);

// useEffect(() => {
//     Axios.get("http://localhost:3001/api/getCategories").then((response) => {
//         setListeServices(response.data);
//     });
// }, []);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(8),
  },
  title: {
    marginBottom: theme.spacing(3),
  },
  h1: {
    textAlign: "start",
  },
  divider: {
    width: "25%",
    marginBottom: theme.spacing(6),
  },
}));

export default function ServicesCards(props) {
  const classes = useStyles();

  const [listeServices, setListeServices] = useState([]);

  const categoryId = props.cardID;

  useEffect(() => {
    Axios.get(
      `http://localhost:3001/api/getServicesByCategories/${categoryId}`
    ).then((response) => {
      setListeServices(response.data);
    });
  }, []);

  return (
    <>
      <div className={classes.root}>
        <h1 className={classes.h1}>Services</h1>
        <Divider className={classes.divider} />

        <br />
        <Grid container justify="center" spacing={3}>
          {listeServices.map((val) => {
            return (
              <ServiceCardItem
                serviceId={val.id}
                serviceName={val.serviceName}
                serviceDesc={val.serviceDesc}
                rating={val.rating}
                path={"/items/" + val.id}
              />
            );
          })}
        </Grid>
      </div>
    </>
  );
}
