import React, { useState, useEffect } from "react";
import Axios from "axios";
import { useAuth } from "../context/auth";
import { Link, Redirect } from "react-router-dom";

import Alert from "@material-ui/lab/Alert";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUpForm() {
  const [isError, setIsError] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setfirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [address, setaddress] = useState("");
  const [tel, setTel] = useState("");
  const [userList, setUserList] = useState([]);
  const { authTokens } = useAuth();
  const { setAuthTokens } = useAuth();
  const classes = useStyles();

  useEffect(() => {
    Axios.get("http://localhost:3001/api/get").then((response) => {
      setUserList(response.data);
    });
  }, []);

  if (authTokens) {
    return <Redirect to="/profileClient" />;
  }

  function validateForm() {
    return (
      // email === /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/ &&
      email.length > 0 &&
      firstName.length >= 2 &&
      lastName.length >= 2 &&
      password.length > 5 &&
      password === confirmPassword
    );
  }

  const submitProfil = () => {
    try {
      Axios.post("http://localhost:3001/api/createAccount", {
        password: password,
        email: email,
        firstName: firstName,
        lastName: lastName,
        address: address,
        tel: tel,
      });
      postLogin();
    } catch (err) {
      setIsError(true);
      console.log(err);
      if (err) {
        console.log(err);
      }
    }

    setUserList([
      ...userList,
      {
        password: password,
        email: email,
        firstName: firstName,
        lastName: lastName,
        address: address,
        tel: tel,
      },
    ]);
  };

  const deleteUser = (user) => {
    Axios.delete(`http://localhost:3001/api/delete/${user}`);
  };

  const postLogin = async (e) => {
    const res = await Axios.post(
      "http://localhost:3001/api/getAccountByLogin",
      {
        accountEmail: email,
        accountPassword: password,
      }
    );
    if (res.data != "") {
      const id = res.data[0].id;
      const res2 = await Axios.get(`http://localhost:3001/api/getVendor/${id}`);
      const account = res.data[0];
      const vendor = res2.data[0];
      const obj = [account, vendor];
      setAuthTokens(obj);
      return <Redirect to="/profileClient" />;
    }
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Inscription
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  type="input"
                  label="Adresse courriel"
                  name="email"
                  autoFocus
                  // autoComplete="email"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  type="none"
                  fullWidth
                  id="firstName"
                  label="Prénom"
                  onChange={(e) => {
                    setfirstName(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  type="none"
                  id="lastName"
                  label="Nom"
                  name="lastName"
                  autoComplete="lname"
                  onChange={(e) => {
                    setlastName(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="adresse"
                  variant="outlined"
                  type="none"
                  fullWidth
                  id="adresse"
                  label="Adresse"
                  onChange={(e) => {
                    setaddress(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="tel"
                  label="Numéro de téléphone"
                  id="tel"
                  type="none"
                  onChange={(e) => {
                    setTel(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Mot de passe"
                  type="password"
                  id="password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  type="password"
                  name="confirmPassword"
                  label="Confirmation du mot de passe"
                  id="confirmPassword"
                  autoComplete="current-password"
                  onChange={(e) => {
                    setConfirmPassword(e.target.value);
                  }}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={submitProfil}
              disabled={!validateForm()}
            >
              S'inscrire
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link to="/sign-in" variant="body2">
                  Vous avez déja un compte? Authentifiez vous.
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          {isError && (
            <Alert severity="error">
              Le courriel ou le mot de passe fourni est incorrect.
            </Alert>
          )}
        </Box>
      </Container>
    </>
  );
}
