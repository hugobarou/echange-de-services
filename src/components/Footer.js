import React from "react";
import { Link } from "react-router-dom";
import "./Footer.css";

function Footer() {
  return (
    <div className="footer-container">
      <section className="footer-subscription">
        <p className="footer-subscription-heading">Viens on est bien.</p>
        <p className="footer-subscription-text">Inscris-toi !!!</p>
      </section>
      <div className="footer-links">
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>About Us</h2>
            <Link to="/sign-up">Sign up</Link>
            <Link to="/">Bonjours</Link>
            <Link to="/">Salut</Link>
            <Link to="/">Allo</Link>
            <Link to="/">Coucou</Link>
          </div>
          <div className="footer-link-items">
            <h2>About Us</h2>
            <Link to="/sign-up">Sign up</Link>
            <Link to="/">Bonjours</Link>
            <Link to="/">Salut</Link>
            <Link to="/">Allo</Link>
            <Link to="/">Coucou</Link>
          </div>
        </div>
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>About Us</h2>
            <Link to="/sign-up">Sign up</Link>
            <Link to="/">Bonjours</Link>
            <Link to="/">Salut</Link>
            <Link to="/">Allo</Link>
            <Link to="/">Coucou</Link>
          </div>
          <div className="footer-link-items">
            <h2>About Us</h2>
            <Link to="/sign-up">Sign up</Link>
            <Link to="/">Bonjours</Link>
            <Link to="/">Salut</Link>
            <Link to="/">Allo</Link>
            <Link to="/">Coucou</Link>
          </div>
        </div>
      </div>
      <section className="social-media">
        <div className="social-media-wrap">
          <div className="footer-logo">
            <Link to="/" className="social-logo">
              CALENDA <i className="far fa-calendar-check"></i>
            </Link>
          </div>
          <small className="website-rights">CALENDA © 2020</small>
          <div className="social-icons">
            <Link
              className="social-icon-link facebook"
              to="/"
              target="_blank"
              aria-label="facebook"
            >
              <i className="fab fa-facebook-f" />
            </Link>
            <Link
              class="social-icon-link instagram"
              to="/"
              target="_blank"
              aria-label="Instagram"
            >
              <i class="fab fa-instagram" />
            </Link>
            <Link
              class="social-icon-link youtube"
              to="/"
              target="_blank"
              aria-label="Youtube"
            >
              <i class="fab fa-youtube" />
            </Link>
            <Link
              class="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="Twitter"
            >
              <i class="fab fa-twitter" />
            </Link>
            <Link
              class="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="LinkedIn"
            >
              <i class="fab fa-linkedin" />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;
