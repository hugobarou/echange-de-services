import Axios from "axios";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import "./Cards.css";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ServiceCardItem from "./ServicesCardsItems.js";
import ItemCardItems from "./ItemCardItems.js";

// const [listeServices, setListeServices] = useState([]);

// useEffect(() => {
//     Axios.get("http://localhost:3001/api/getCategories").then((response) => {
//         setListeServices(response.data);
//     });
// }, []);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(8),
  },
  title: {
    marginBottom: theme.spacing(3),
  },
}));

export default function ServicesCards(props) {
  const classes = useStyles();

  const [listeItems, setListeItems] = useState([]);

  const Id = props.cardID;

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getItems/${Id}`).then((response) => {
      setListeItems(response.data);
    });
  }, []);

  return (
    <>
      <div className={classes.root}>
        <h1>Les items</h1>
        <br />
        <Grid container justify="center" spacing={3}>
          {listeItems.map((val) => {
            return (
              <ItemCardItems
                itemPicture={val.itemPicture}
                itemName={val.itemName}
                itemDesc={val.itemDesc}
                itemPrice={val.itemPrice}
              />
            );
          })}
        </Grid>
      </div>
    </>
  );
}
