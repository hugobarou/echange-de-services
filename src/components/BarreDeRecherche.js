import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1),
    width: "75%%",
  },
  submit: {
    justifyContent: "flex-start",
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "#6c757d",
    },
  },
  // root: {
  //   padding: '2px 4px',
  //   width: 400,
  // },
  // input: {
  //   marginLeft: theme.spacing(1),
  //   flex: 1,
  // },
  // iconButton: {
  //   padding: 10,
  // },
}));

export default function BarreDeRecherche() {
  const [desc, setDesc] = useState("");
  const classes = useStyles();
  const [isError, setIsError] = useState(false);

  const rechercher = () => {
    return <Link to={`/recherche/${desc}`}></Link>;
  };
  return (
    // <Paper component="form" className={classes.root}>
    //   <InputBase placeholder="Chercher"
    //     className={classes.input}
    //   >
    //   </InputBase>
    //   <IconButton>
    //     <SearchIcon />
    //   </IconButton>
    // </Paper>
    <div>
      <form noValidate autoComplete="off">
        <TextField
          id="outlined-basic"
          className={classes.root}
          type="search"
          variant="outlined"
          onChange={(e) => {
            setDesc(e.target.value);
          }}
        />
        <Button variant="contained" color="primary" href="#contained-buttons">
          <Link to={`/recherche/${desc}`}>Rechercher</Link>
        </Button>
        <Box mt={5}></Box>
      </form>
    </div>
  );
}
