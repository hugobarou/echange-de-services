import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button } from "./Button";
import "./Navbar.css";
import { useAuth } from "./../context/auth";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  btnlg: {
    margin: "none !important",
  },
}));

export default function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);
  const [isLoggedIn, setLoggedIn] = useState(false);
  const { authTokens } = useAuth();
  const classes = useStyles();

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };


  const showButtonNav = () => {
    if (authTokens) {
      return [
        <Button classeName={classes.btnlg} buttonStyle="btn--outline" to="/ProfileClient">
          Profil
        </Button>,
        <Button classeName={classes.btnlg} buttonStyle="btn--outline" to="/sign-out" whiteSpace="nowrap">
          Se déconnecter
        </Button>,
      ];
    } else {
      return [
        <Button classeName={classes.btnlg} buttonStyle="btn--outline" to="/sign-up">
          S'inscrire
        </Button>,
        <Button classeName={classes.btnlg} buttonStyle="btn--outline" to="/sign-in">
          Se connecter
        </Button>,
      ];
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener("resize", showButton);

  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <Link to="/" className="navbar-logo">
            CALENDA <i className="far fa-calendar-check" />
          </Link>
          <div className="menu-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"} />
          </div>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                Accueil
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-links" onClick={closeMobileMenu}>
                À propos
              </Link>
            </li>
            <li className="nav-item">
              {authTokens ? (
                <Link
                  to="/ProfileClient"
                  className="nav-links-mobile"
                  onClick={closeMobileMenu}
                >
                  Profil
                </Link>
              ) : (
                  <Link
                    to="/sign-up"
                    className="nav-links-mobile"
                    onClick={closeMobileMenu}
                  >
                    S'inscrire
                  </Link>
                )}
              <li className="nav-item">
                {authTokens ? (
                  <Link
                    to="/sign-out"
                    className="nav-links-mobile"
                    onClick={closeMobileMenu}
                  >
                    Se déconnecter
                  </Link>
                ) : (
                    <Link
                      to="/sign-in"
                      className="nav-links-mobile"
                      onClick={closeMobileMenu}
                    >
                      Se connecter
                    </Link>
                  )}
              </li>
            </li>
          </ul>
          {button && showButtonNav()}
        </div>
      </nav>
    </>
  );
}

