/*
 * Programmeur: Hugo Barou
 * Fichier: ServiceForm.js
 * Description: Formulaire pour créer le service du vendor.
 */

// Importations
import React, { useState, useEffect } from "react";
import Footer from "./Footer.js";
import Axios from "axios";
import "./ServiceForm.css";

export default function ServiceForm(props) {
  const vendorID = props.vendor;
  const [categories, setCategories] = useState([]);
  const [name, setName] = useState("");
  const [desc, setDesc] = useState("");
  const [category, setCategory] = useState("");
  const [location, setLocation] = useState("");

  console.log(vendorID);
  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getCategories/`).then((response) => {
      setCategories(response.data);
    });
  }, []);

  const createService = () => {
    try {
      Axios.post(`http://localhost:3001/api/insertService/`, {
        serviceName: name,
        serviceDesc: desc,
        categoryID: category,
        vendorID: vendorID,
        location: location,
      });
    } catch (err) {
      console.log(err.response.data.msg);
    }
  };

  return (
    <>
      <div className="service">
        <div className="container">
          <form className="service-form">
            <label for="serviceName">Nom du service:</label>
            <input
              type="text"
              id="serviceName"
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
            <label for="serviceDesc">Description du service:</label>
            <input
              type="text"
              id="serviceDesc"
              onChange={(e) => {
                setDesc(e.target.value);
              }}
            />
            <label for="serviceCategory">Catégorie du service:</label>
            <select
              id="serviceCategory"
              onChange={(e) => {
                setCategory(e.target.value);
              }}
            >
              {categories.map((val) => {
                return <option value={val.id}>{val.nom}</option>;
              })}
            </select>
            <label for="location">Adresse du service:</label>
            <input
              type="text"
              id="location"
              onChange={(e) => {
                setLocation(e.target.value);
              }}
            />
            <input
              type="submit"
              value="Créer votre service"
              onClick={createService}
            />
          </form>
        </div>
      </div>
    </>
  );
}
