/*
 * Programmeur: Hugo Barou
 * Fichier: VendorProfile.js
 * Description: Affichage d'un profile Vendor.
 */

// Importation
import React, { useState } from "react";
import { Avatar } from "@material-ui/core";
import Axios from "axios";
// import "./VendorProfile.css";
// import Footer from "./Footer.js";
import { Link } from "react-router-dom";
// import "../App.css";
import { useAuth } from "../context/auth";

export default function VendorProfile() {
  const { setAuthTokens } = useAuth();
  const { authTokens } = useAuth();
  const vendor = authTokens[1];
  const id = authTokens[1].account_id;
  const [avatar, setAvatar] = useState([]);

  const fileSelected = (e) => {
    setAvatar(e.target.files[0]);
  };

  // Update
  const updateAvatar = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("avatar", avatar);
    formData.set("accountId", id);

    try {
      await Axios.put("http://localhost:3001/api/updateVendor", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      const res = Axios.get(`http://localhost:3001/api/getVendor/${id}`);
      const account = authTokens[0];
      const vendor = res.data[0];
      const obj = [account, vendor];
      setAuthTokens(obj);
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      } else {
        console.log(err.response.data.msg);
      }
    }
  };
  // Delete
  const deleteVendor = () => {
    try {
      Axios.delete(`http://localhost:3001/api/deleteVendor/${id}`);
    } catch (err) {
      alert(err);
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      } else {
        console.log(err.response.data.msg);
      }
    }
  };

  return (
    <>
      <div className="vendor-profile">
        <div className="container">
          <Avatar
            src={vendor.profile_img}
            style={{ width: "250px", height: "250px" }}
          ></Avatar>
          <form className="form">
            <p>Change avatar</p>
            <input
              type="file"
              id="avatar"
              accept="image/png, image/jpeg, image/jpg"
              onChange={fileSelected}
            />
            <input
              type="submit"
              value="Mise à jour de votre profil "
              onClick={updateAvatar}
            />
            <input
              type="submit"
              value="Supprimer votre profil"
              onClick={deleteVendor}
            />
            {/* <Link to="/vendor-service">Votre Service</Link>
            <p></p>
            <Link to="/favorite">Vos favoris</Link> */}
          </form>
        </div>
      </div>
      {/* <Footer /> */}
    </>
  );
}
