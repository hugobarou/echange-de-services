/*
 * Programmeur: Hugo Barou
 * Fichier: ItemInfo.js
 * Description: afficher, supprimer, modifier les items dans un service.
 */

// Importations
import React, { useState, useEffect } from "react";
import { Avatar } from "@material-ui/core";
import Axios from "axios";
import "./ItemInfo.css";

export default function ItemForm(props) {
  const [path, setPath] = useState(props.picture);
  const [name, setName] = useState(props.name);
  const [desc, setDesc] = useState(props.desc);
  const [price, setPrice] = useState(props.price);
  const [serviceName, setServiceName] = useState(props.serviceName);

  return (
    <div className="list-item">
      <h1>Service : {serviceName}</h1>
      <Avatar src={path} style={{ width: "250px", height: "250px" }}></Avatar>
      <h1>Nom : {name}</h1>
      <h1>Description :{desc}</h1>
      <h1>Prix : {price}</h1>
      {/* <label>Nom de l'item:</label>
            <input type='text' placeholder={name} onChange={(e) => {setName(e.target.value)}}/>
            <label>Description de l'item:</label>
            <input type='text' placeholder={desc} onChange={(e) => {setDesc(e.target.value)}}/>
            <label>Prix de l'item:</label>
            <input type='text' placeholder={price} onChange={(e) => {setPrice(e.target.value)}}/> */}
    </div>
  );
}
