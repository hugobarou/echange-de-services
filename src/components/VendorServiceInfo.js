/*
 * Programmeur: Hugo Barou
 * Fichier: VendorServiceInfo.js
 * Description: La vue pour le service d'un vendor.
 */

// Importations
import { BrowserRouter as Link } from "react-router-dom";
import React, { useState, useEffect } from "react";

import Axios from "axios";
import "./VendorServiceInfo.css";
import ItemForm from "./ItemForm";

export default function VendorServiceInfo(props) {
  const service = props.service[0];
  const [categories, setCategories] = useState([]);
  const [categoryName, setCategoryName] = useState("");
  const [name, setName] = useState(service.serviceName);
  const [desc, setDesc] = useState(service.serviceDesc);
  const [category, setCategory] = useState(service.categoryID);
  const [location, setLocation] = useState(service.location);

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getCategories/`).then((response) => {
      setCategories(response.data);
    });
    Axios.get(`http://localhost:3001/api/getCategoryName/${category}`).then(
      (response) => {
        setCategoryName(response.data[0].nom);
      }
    );
  }, []);

  const deleteService = () => {
    const id = service.id;
    try {
      Axios.delete(`http://localhost:3001/api/deleteService/${id}`);
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      } else {
        console.log(err.response.data.msg);
      }
    }
  };

  const updateService = () => {
    try {
      Axios.put("http://localhost:3001/api/updateService", {
        serviceID: service.id,
        serviceName: name,
        serviceDesc: desc,
        categoryID: category,
        location: location,
      });
    } catch (err) {
      console.log(err.response.data.msg);
    }
  };

  return (
    <>
      <div className="vendor-service">
        <div className="container">
          <form className="vendor-service-form">
            <label for="serviceName">Nom du service:</label>
            <input
              type="text"
              id="serviceName"
              placeholder={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
            <label for="serviceDesc">Description du service:</label>
            <input
              type="text"
              id="serviceDesc"
              placeholder={desc}
              onChange={(e) => {
                setDesc(e.target.value);
              }}
            />
            <label for="serviceCategory">Catégorie du service:</label>
            <select
              id="serviceCategory"
              onChange={(e) => {
                setCategory(e.target.value);
              }}
            >
              <option value={category}>{categoryName}</option>
              {categories.map((val) => {
                return <option value={val.id}>{val.nom}</option>;
              })}
            </select>
            <label for="location">Adresse du service:</label>
            <input
              type="text"
              id="location"
              placeholder={location}
              onChange={(e) => {
                setLocation(e.target.value);
              }}
            />
            {/* <label>Rating:{service.rating}</label> */}
            <input
              type="submit"
              value="Mise à jour de vos informations"
              onClick={updateService}
            />
            <input
              type="submit"
              value="Supprimer votre service"
              onClick={deleteService}
            />
          </form>
        </div>
        <ItemForm serviceID={service.id} />
      </div>
    </>
  );
}
