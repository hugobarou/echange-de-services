/*
 * Programmeur: Hugo Barou
 * Fichier: ItemInfo.js
 * Description: afficher, supprimer, modifier les items dans un service.
 */

// Importations
import React, { useState } from "react";
import { Avatar } from "@material-ui/core";
import Axios from "axios";
import "./ItemInfo.css";

export default function ItemForm(props) {
  const [picture, setPicture] = useState([]);
  const [itemId, setItemId] = useState(props.id);
  const [path, setPath] = useState(props.picture);
  const [name, setName] = useState(props.name);
  const [desc, setDesc] = useState(props.desc);
  const [price, setPrice] = useState(props.price);

  const fileSelected = (e) => {
    setPicture(e.target.files[0]);
  };

  const deleteItem = () => {
    const id = itemId;
    try {
      Axios.delete(`http://localhost:3001/api/deleteItem/${id}`);
    } catch (err) {
      console.log(err.response.data.msg);
    }
  };

  const updateItem = (e) => {
    e.preventDefault();
    const formData = new FormData();

    formData.append("picture", picture);
    formData.set("itemId", itemId);
    formData.set("itemName", name);
    formData.set("itemDesc", desc);
    formData.set("itemPrice", price);
    try {
      Axios.put("http://localhost:3001/api/updateItem", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      console.log(err.response.data.msg);
    }
  };

  return (
    <div className="list-item">
      <Avatar src={path} style={{ width: "250px", height: "250px" }}></Avatar>
      <input
        type="file"
        accept="image/png, image/jpeg, image/jpg"
        onChange={fileSelected}
      />
      <label>Nom de l'item:</label>
      <input
        type="text"
        placeholder={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <label>Description de l'item:</label>
      <input
        type="text"
        placeholder={desc}
        onChange={(e) => {
          setDesc(e.target.value);
        }}
      />
      <label>Prix de l'item:</label>
      <input
        type="text"
        placeholder={price}
        onChange={(e) => {
          setPrice(e.target.value);
        }}
      />
      <input type="submit" value="Mise à jour" onClick={updateItem} />
      <input type="submit" value="Supprimer" onClick={deleteItem} />
    </div>
  );
}
