import Axios from "axios";
import React, { useState, useEffect } from "react";
import CardItem from "./CardItem";

export default function Cards(props) {
  const [listeCategorie, setListeCategorie] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3001/api/getCategories").then((response) => {
      setListeCategorie(response.data);
    });
  }, []);

  const verif = (id) => {
      const src = "";
    //const id = val.id;
    if (id >= 9) {
        src={"images/":+id+".jpg"}
    }
    else {
        src="images/img-3.jpg"
    }
    return src;
  }

  return (
    <div className="cards">
      <h1>Catégories</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            {listeCategorie.map((val, index, arr) => {
              return (
                <CardItem
                  text={val.nom}
                  src={"images/"+val.id+".jpg"}
                  //src="images/img-3.jpg"
                  label="About"
                  path={"/services/" + val.id}
                />
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}
