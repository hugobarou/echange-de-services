import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";
import { useAuth } from "../../context/auth";

import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import FavoriteIcon from "@material-ui/icons/Favorite";
import StorefrontIcon from "@material-ui/icons/Storefront";
import Vendor from "./Vendor.js";
import Favorite from "./Favorite.js";
import VendorService from "./VendorService";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import Footer from "../Footer";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    justifyContent: "flex-start",
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "#6c757d",
    },
  },
  appBar: {
    backgroundColor: "#6c757d",
    // height: "80px",
  },
  tabs: {
    indicator: "black",
    color: "black",
  },
  // btn: {
  // 	backgroundColor: "#6c757d",
  // },
}));

export default function ProfileClient() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const { authTokens } = useAuth();
  const [accountID, setAccountID] = useState(authTokens[0].id);
  const [accountFirstName, setAccountFirstName] = useState(
    authTokens[0].firstName
  );
  const [accountLastName, setAccountLastName] = useState(
    authTokens[0].lastName
  );
  const [accountAddress, setAccountAddress] = useState(authTokens[0].address);
  const [accountEmail, setAccountEmail] = useState(authTokens[0].email);
  const [accountPassword, setAccountPassword] = useState(
    authTokens[0].password
  );
  const [accountTel, setAccountTel] = useState(authTokens[0].tel);
  const [confirmPassword, setConfirmPassword] = useState("");
  const classes = useStyles();

  const id = authTokens[0].id;
  const { setAuthTokens } = useAuth();
  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getVendor/${id}`).then((response) => {
      const vendor = response.data[0];
      const account = authTokens[0];
      const obj = [account, vendor];
      setAuthTokens(obj);
    });
  }, []);

  const submitChanges = async (e) => {
    const res = await Axios.put("http://localhost:3001/api/updateAccount", {
      accountID: accountID,
      accountFirstName: accountFirstName,
      accountLastName: accountLastName,
      accountAddress: accountAddress,
      accountEmail: accountEmail,
      accountPassword: authTokens[0].password,
      accountTel: accountTel,
    });
  };

  const submitPassword = async (e) => {
    const res = await Axios.put("http://localhost:3001/api/updateAccount", {
      accountID: accountID,
      accountFirstName: authTokens[0].firstName,
      accountLastName: authTokens[0].lastName,
      accountAddress: authTokens[0].address,
      accountEmail: authTokens[0].email,
      accountPassword: accountPassword,
      accountTel: authTokens[0].tel,
    });
  };

  function validateForm1() {
    return (
      // email === /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/ &&
      accountEmail.length > 0 &&
      accountFirstName.length >= 2 &&
      accountLastName.length >= 2
    );
  }

  function checkEmail() {
    return accountEmail.length > 0;
  }

  function checkFName() {
    return accountFirstName.length >= 2;
  }

  function checkLName() {
    return accountLastName.length >= 2;
  }

  function validateForm2() {
    return accountPassword.length > 5 && accountPassword === confirmPassword;
  }

  return (
    <>
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Tabs
            className={classes.tabs}
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab
              label="Mon compte"
              icon={<AccountCircleIcon />}
              {...a11yProps(0)}
            />
            <Tab label="Favoris" icon={<FavoriteIcon />} {...a11yProps(1)} />
            {/* <Tab label="Compte vendeur" icon={ } {...a11yProps(2)} /> */}
            <Tab
              label="Compte vendeur"
              icon={<AccountBoxIcon />}
              {...a11yProps(2)}
            />
            {authTokens[1] != null ? (
              <Tab
                label="Mes Services"
                icon={<StorefrontIcon />}
                {...a11yProps(3)}
              />
            ) : (
              <Tab
                label="Mes Services"
                icon={<StorefrontIcon />}
                {...a11yProps(3)}
                disabled
              />
            )}
          </Tabs>
        </AppBar>

        <TabPanel value={value} index={0}>
          <Container component="main" maxWidth="lg">
            <CssBaseline />
            <div className={classes.paper}>
              <form className={classes.form} noValidate>
                <Typography component="h1" variant="h4" align="left">
                  Paramètres de profil
                </Typography>
                <Typography component="h1" variant="h6" align="left">
                  Vous pouvez modifier vos informations de compte ici
                </Typography>
                <Divider />
                <br />
                <Grid container spacing={2}>
                  <Grid item xs={12} md={3}>
                    <Typography component="h1" variant="h5" align="left">
                      Prénom
                    </Typography>
                    <TextField
                      variant="outlined"
                      fullWidth
                      required
                      id="accountFirstName"
                      type="none"
                      defaultValue={accountFirstName}
                      error={!checkFName()}
                      onChange={(e) => {
                        setAccountFirstName(e.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} md={3}>
                    <Typography component="h1" variant="h5" align="left">
                      Nom
                    </Typography>
                    <TextField
                      variant="outlined"
                      fullWidth
                      required
                      id="accountLastName"
                      type="none"
                      defaultValue={accountLastName}
                      error={!checkLName()}
                      onChange={(e) => {
                        setAccountLastName(e.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Typography component="h1" variant="h5" align="left">
                      Adresse courriel
                    </Typography>
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="accountEmail"
                      type="none"
                      defaultValue={accountEmail}
                      error={!checkEmail()}
                      onChange={(e) => {
                        setAccountEmail(e.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Typography component="h1" variant="h5" align="left">
                      Adresse
                    </Typography>
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="accountAddress"
                      type="none"
                      defaultValue={accountAddress}
                      onChange={(e) => {
                        setAccountAddress(e.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Typography component="h1" variant="h5" align="left">
                      Numéro de téléphone
                    </Typography>
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="accountTel"
                      type="none"
                      defaultValue={accountTel}
                      onChange={(e) => {
                        setAccountTel(e.target.value);
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={2} justifyContent="flex-end">
                  <Grid item xs={12} md={1}>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      onClick={submitChanges}
                      disabled={!validateForm1()}
                    >
                      Appliquer
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
            <Box mt={5} />
          </Container>

          <Container component="main" maxWidth="lg">
            <CssBaseline />
            <div className={classes.paper}>
              <form className={classes.form} noValidate>
                <Grid item xs={12} md={6}>
                  <Typography component="h1" variant="h4" align="left">
                    Mot de passe
                  </Typography>
                  <Typography component="h1" variant="h6" align="left">
                    Vous pouvez modifier votre mot de passe ici
                  </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Divider />
                  <br />
                  <Typography component="h1" variant="h5" align="left">
                    Nouveau mot de passe
                  </Typography>
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="accountPassword"
                    type="password"
                    // error={validateForm2()}
                    onChange={(e) => {
                      setAccountPassword(e.target.value);
                    }}
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <Typography component="h1" variant="h5" align="left">
                    Confirmer le mot de passe
                  </Typography>
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="confirmPassword"
                    type="password"
                    // error={validateForm2()}
                    onChange={(e) => {
                      setConfirmPassword(e.target.value);
                    }}
                  />
                </Grid>
                <Grid container spacing={2} justifyContent="flex-end">
                  <Grid item xs={12} md={1}>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      onClick={submitPassword}
                      disabled={!validateForm2()}
                    >
                      Sauvegarder
                    </Button>
                  </Grid>
                </Grid>
                {/* <Grid item xs={24} md={8}>
								<Button
									type="submit"
									variant="contained"
									color="primary"
									className={classes.submit}
									onClick={submitPassword}
									disabled={!validateForm2()}
								>
									Sauvegarder
			</Button>
							</Grid> */}
              </form>
            </div>
            <Box mt={5} />
          </Container>
          {console.log("AFFICHAGE FAVORIS")}
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Favorite />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Vendor />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <VendorService />
        </TabPanel>
      </div>
      <Footer />
    </>
  );
}
