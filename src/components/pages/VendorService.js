/*
 * Programmeur: Hugo Barou
 * Fichier: Service.js
 * Description: La page pour un service.
 */

// Importations
import React, { useState, useEffect } from "react";
import ServiceForm from "../ServiceForm.js";
import VendorServiceInfo from "../VendorServiceInfo.js";
import Axios from "axios";
import { useAuth } from "../../context/auth";

export default function VendorService() {
  const { authTokens } = useAuth();
  const [service, setService] = useState([]);
  const id = authTokens[1].id;
  console.log("ID : " + id);
  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getService/${id}`).then((response) => {
      setService(response.data);
      console.log(service);
    });
  }, []);

  return service != "" && service != undefined ? (
    <VendorServiceInfo service={service} />
  ) : (
    <ServiceForm vendor={id} />
  );
}
