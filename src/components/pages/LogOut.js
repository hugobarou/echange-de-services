import React from "react";
import { Redirect } from "react-router-dom";
import "../../App.css";
import { useAuth } from "../../context/auth";

export default function Logout() {
  const { setAuthTokens } = useAuth();
  setAuthTokens("");

  return <Redirect to="/sign-in" />;
}
