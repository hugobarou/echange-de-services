/*
 * Programmeur: Hugo Barou
 * Fichier: Vendor.js
 * Description: La page d'un vendeur
 */

// Importations
import React, { useState, useEffect } from "react";
import Axios from "axios";
import "../../App.js";
import VendorProfile from "../VendorProfile.js";
import VendorForm from "../VendorForm.js";
import { useAuth } from "../../context/auth";

export default function Vendor() {
  const { authTokens } = useAuth();
  const [vendor, setVendor] = useState(authTokens[1]);
  const { setAuthTokens } = useAuth();
  const id = authTokens[0].id;
  // console.log(authTokens);
  // console.log(vendor);

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getVendor/${id}`).then((response) => {
      const vendor = response.data[0];
      const account = authTokens[0];
      const obj = [account, vendor];
      setAuthTokens(obj);
    });
  }, []);

  return authTokens[1] != null ? <VendorProfile /> : <VendorForm />;
}
