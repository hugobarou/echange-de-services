import Axios from "axios";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import "../Cards.css";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ItemClient from "../ItemClient";
import { Redirect } from "react-router-dom";
import ServiceCardItem from "../ServicesCardsItems";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(8),
  },
  title: {
    marginBottom: theme.spacing(3),
  },
}));

export default function Recherche(props) {
  const classes = useStyles();

  const [listeItems, setListeItems] = useState([]);
  const [desc, setDesc] = useState(props.match.params.desc);
  const retour = "Aucun résultat";
  // const [listeServices, setListeServices] = useState([]);

  // useEffect(() => {
  //   Axios.get(
  //     `http://localhost:3001/api/getServicesByCategories/${categoryId}`
  //   ).then((response) => {
  //     setListeServices(response.data);
  //   });
  // }, []);

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/findItem/${desc}`).then((response) => {
      setListeItems(response.data);
      console.log(listeItems);
    });
  }, []);

  if (listeItems.length < 0) {
    return alert(retour), (<Redirect to="/" />);
  }

  return (
    <>
      <div className={classes.root}>
        <Typography
          className={classes.title}
          variant="h4"
          marked="center"
          align="center"
          component="h2"
        >
          ITEMS
        </Typography>
        <Grid container justify="center" spacing={3}>
          {listeItems.map((val) => {
            console.log(val.serviceDesc);

            return (
              // <ItemClient
              //   picture={val.itemPicture}
              //   name={val.itemName}
              //   desc={val.itemDesc}
              //   price={val.itemPrice}
              //   serviceName={val.serviceName}

              // />
              <ServiceCardItem
                // imgUrl={`https://source.unsplash.com/random?sig=${index}`}
                serviceId={val.id}
                serviceName={val.serviceName}
                serviceDesc={val.serviceDesc}
                rating={val.rating}
                path={"/items/" + val.id}
              />
            );
          })}
        </Grid>
      </div>
    </>
  );
}
