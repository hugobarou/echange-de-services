import React from "react";

import ServicesCards from "../ServicesCards.js";
import Footer from "../Footer.js";

function Services(props) {
  return (
    <>
      <ServicesCards cardID={props.match.params.id} />
      <Footer />
    </>
  );
}

export default Services;
