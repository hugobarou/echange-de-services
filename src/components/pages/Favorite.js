import React, { useState, useEffect } from "react";
import Axios from "axios";
import { useAuth } from "../../context/auth";
import ServiceCardItem from "../ServicesCardsItems.js";
import Grid from "@material-ui/core/Grid";

export default function Favorite() {
  const { authTokens } = useAuth();
  const [favorites, setFavorites] = useState([]);
  const [id, setId] = useState(authTokens[0].id);

  useEffect(() => {
    Axios.get(`http://localhost:3001/api/getFavorites/${id}`).then(
      (response) => {
        setFavorites(response.data);
      }
    );
  }, []);

  return (
    <>
      {/* {console.log("Affichage Favoris")} */}
      <div>
        <h1>Vos favoris :</h1>

        <Grid container justify="center" spacing={3}>
          {favorites.map((val) => {
            return (
              <ServiceCardItem
                serviceId={val.id}
                serviceName={val.serviceName}
                serviceDesc={val.serviceDesc}
                rating={val.rating}
                path={"/items/" + val.id}
              />
            );
          })}
        </Grid>
      </div>
    </>
  );
}
