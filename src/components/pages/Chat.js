import React from "react";

import "../../App.js";
import Footer from "../Footer.js";
import ChatForm from "../ChatForm";

export default function Chat() {
  return (
    <>
      <ChatForm />
      <Footer />
    </>
  );
}
