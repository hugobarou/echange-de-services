import React from "react";
import Footer from "../Footer.js";
import SignUpForm from "../SignUpForm";

function SignUp() {
  return (
    <>
      <SignUpForm />
      <Footer />
    </>
  );
}

export default SignUp;
