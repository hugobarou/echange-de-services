import React from "react";

import ItemsCards from "../ItemsCards.js";
import Footer from "../Footer.js";

function Items(props) {
  return (
    <>
      <ItemsCards cardID={props.match.params.id} />
      <Footer />
    </>
  );
}

export default Items;
