import React from "react";

import Cards from "../Cards.js";
import Footer from "../Footer.js";
import HeroSection from "../HeroSection";
import BarreDeRecherche from "../BarreDeRecherche.js";
function Home() {
  return (
    <>
      <HeroSection />
      <BarreDeRecherche />
      <Cards />
      <Footer />
    </>
  );
}

export default Home;
