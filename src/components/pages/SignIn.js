import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Alert from '@material-ui/lab/Alert';


import { makeStyles } from "@material-ui/core/styles";
import { Error } from "../AuthForms";
import { useAuth } from "../../context/auth";

import Footer from "../Footer";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(2),
  },
  submit: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function Login() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [accountEmail, setAccountEmail] = useState("");
  const [accountPassword, setAccountPassword] = useState("");
  const { authTokens } = useAuth();
  const { setAuthTokens } = useAuth();
  const classes = useStyles();

  const postLogin = async (e) => {
    const res = await axios.post(
      "http://localhost:3001/api/getAccountByLogin",
      {
        accountEmail: accountEmail,
        accountPassword: accountPassword,
      }
    );
    if (res.data != "") {
      const id = res.data[0].id;
      const res2 = await axios.get(`http://localhost:3001/api/getVendor/${id}`);
      const account = res.data[0];
      const vendor = res2.data[0];
      const obj = [account, vendor];
      setAuthTokens(obj);
      setLoggedIn(true);
    } else {
      setIsError(true);
    }
  };

  function validateForm() {
    return (
      accountEmail.length > 0 &&
      accountPassword.length > 0
    );
  }

  if (isLoggedIn) {
    return <Redirect to="/ProfileClient" />;
  }
  if (authTokens) {
    return <Redirect to="/ProfileClient" />;
  }

  return (
    <>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Authentification
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                type="email"
                value={accountEmail}
                onChange={(e) => {
                  setAccountEmail(e.target.value);
                }}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Adresse courriel"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                type="password"
                value={accountPassword}
                onChange={(e) => {
                  setAccountPassword(e.target.value);
                }}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Mot de passe"
                id="password"
                autoComplete="current-password"
              />
              <Button
                // type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={postLogin}
                disabled={!validateForm()}
              >
                Se connecter
              </Button>
              <Grid container>
                <Grid item>
                  <Link to="/sign-up" variant="body2">
                    {"Vous n'avez pas de compte?"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}>
                {isError && (
                  <Alert severity="error">
                    Le courriel ou le mot de passe fourni est incorrect.
                  </Alert>
                )}
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
      <Footer />
    </>
  );
}

// export default Login;
