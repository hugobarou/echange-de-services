import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Rating from "@material-ui/lab/Rating";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { useAuth } from "../context/auth";
import Axios from "axios";

// const styles =
// {

//     media: {
//         height: 0,
//         paddingTop: '56.25%', // 16:9,
//         marginTop: '30'
//     }
// };

const useStyles = makeStyles((theme) => ({
  root: {
    // maxHeight: 400,
    // display:"flex",
  },
  media: {
    height: 250,
  },
  cardLinks: {
    color: theme.palette.secondary.dark,
  },
  serviceDesc: {
    height: "150px",
  },
  stars: {
    alignItem: "center",
  },
  desc: {
    textAlign: "start",
  },
}));

function getRandomNumber() {
  return Math.floor(Math.random() * 10);
}
export default function ServicesCardsItems(props) {
  const classes = useStyles();
  const { authTokens } = useAuth();
  // const [id, setId] = useState(authTokens[0].id);
  //   const [id, setId] = useState(authTokens[0].id);

  const ajouterFavoris = () => {
    const accountId = authTokens[0].id;
    const serviceId = props.serviceId;
    try {
      Axios.post("http://localhost:3001/api/insertFavorite", {
        accountId: accountId,
        serviceId: serviceId,
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      } else {
        console.log(err.response.data.msg);
      }
    }
  };

  const deleteFavorite = () => {
    try {
      Axios.post("http://localhost:3001/api/deleteFavorite", {
        accountId: authTokens[0].id,
        serviceId: props.serviceId,
      });
    } catch (err) {
      if (err.response.status === 500) {
        console.log("There was a problem with the server");
      } else {
        console.log(err.response.data.msg);
      }
    }
  };

  const affichageBtn = () => {
    if (window.location.pathname === "/ProfileClient") {
      return [
        <Button onClick={deleteFavorite}>Supprimer des favoris</Button>,

        console.log(window.location.pathname),
      ];
    } else if (authTokens && window.location.pathname !== "/ProfileClient") {
      return [
        <Button onClick={ajouterFavoris}>Ajouter aux favoris</Button>,
        // console.log("elseIf"),
      ];
    } else {
      return console.log("else");
    }
  };

  return (
    <>
      <Grid item xs={12} sm={5}>
        <Card className={classes.root}>
          <CardActionArea>
            <Link className="service__item__link" to={props.path}>
              <CardMedia
                className={classes.media}
                // image={require('./z0RgblR.jpg')}
                // src={props.imgUrl}
                src={`https://source.unsplash.com/random?sig=${getRandomNumber()}`}
                component="img"
                // title="Contemplative Reptile"
              />
            </Link>
            <CardContent className={classes.serviceDesc}>
              <Typography gutterBottom variant="h5" component="h2">
                {props.serviceName}
              </Typography>
              <Typography
                className={classes.desc}
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {props.serviceDesc}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Rating name="read-only" value={props.rating} readOnly />
            {affichageBtn()}
          </CardActions>
        </Card>
      </Grid>
    </>
  );
}
